from constants import OBJECTS, OBJECTS_CHAR
from random import choice, randrange


def gen_field(n, m):
    field = []
    rand_arr = ['empty_cell']
    for i in range(n):
        field.append([])
        for j in range(m):
            field[-1].append(choice(rand_arr))

    return field


def gen_password(ln=10):
    password = ''
    left = ord('a')
    right = ord('z')
    for i in range(ln):
        plus = chr(randrange(left, right + 1))
        while plus == '':
            plus = chr(randrange(left, right + 1))
        password += plus
    return password


if __name__ == "__main__":
    print(gen_password())
EMPTY_WORD = 'empty_cell'
BUSH_WORD = 'bush'
ROCK_WORD = 'rock'
PLAYER_WORD = 'player'
ABROAD_WORD = 'abroad'

EMPTY_CHAR = '.'
BUSH_CHAR = '*'
ROCK_CHAR = '+'
PLAYER_CHAR = 'P'
ABROAD_CHAR = '-'

OBJECTS = [EMPTY_WORD, BUSH_WORD, ROCK_WORD, PLAYER_WORD, ABROAD_WORD]

OBJECTS_CHAR = {EMPTY_WORD: EMPTY_CHAR,
                BUSH_WORD: BUSH_CHAR,
                ROCK_WORD: ROCK_CHAR,
                PLAYER_WORD: PLAYER_CHAR,
                ABROAD_WORD: ABROAD_CHAR}

CHAR_OBJECTS = {EMPTY_CHAR: EMPTY_WORD,
                BUSH_CHAR: BUSH_WORD,
                ROCK_CHAR: ROCK_WORD,
                PLAYER_CHAR: PLAYER_WORD,
                ABROAD_CHAR: ABROAD_WORD}

MAX_PLAYER_MOVE_ONE_STEP = 1
VISION_LEN = 3
PLAYER_CANT_MOVE_MORE_THAN = 'player can\'t move more than ' + str(
    MAX_PLAYER_MOVE_ONE_STEP) + ' one step'
WRONG_PASSWORD = 'wrong password'

SEPARATOR = '$'
COMMAND_SEPARATOR = '%'

PORT = 9009


def PLAYER_WITH_ID_NOT_FOUND(id):
    return 'player with id ' + str(id) + ' not found'


if __name__ == "__main__":
    print(OBJECTS_CHAR[ABROAD_WORD])

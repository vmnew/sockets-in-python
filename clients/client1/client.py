from constants import *
import socket


def new_socket():
    sock = socket.socket()
    sock.connect(('localhost', PORT))
    return sock


def recv(sock, ln=32768):
    ret = sock.recv(ln).decode('utf-8')
    print('data in recv =', ret)
    return ret


def get(command):
    sock = new_socket()
    sock.send(command.encode())
    data = recv(sock)
    sock.close()
    print('data in get =', data)
    return data


def show(arr):
    print(arr)
    arr = arr.rstrip(']]').lstrip('[[')
    print(arr)
    arr = arr.split('], [')
    print(arr)
    print()
    for i in arr:
        print(i)


data = get('-' + SEPARATOR + '-' + SEPARATOR + 'create')
print('data =', data)
show(data.split(SEPARATOR)[-1])
id, password = data.split(SEPARATOR)[:-1:]
id = int(id)

while True:
    input()
    arr = get(SEPARATOR.join([str(id), password, 'show']))
    show(arr)




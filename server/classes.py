import socket
from functions import *
from constants import *
import asyncio


class Server:
    def __init__(self, port=PORT, listen=11, n=10, m=10):
        self.sock = socket.socket()
        self.sock.bind(('', port))
        self.sock.listen(listen)
        self.game = Game(n, m)

    async def run(self):
        while True:
            conn, addr = await self.sock.accept()
            self.processing_request(conn, addr)

    def processing_request(self, conn, addr):
        try:
            data = conn.recv(32768).decode('utf-8')

            print('data =', data)

            id, password, command = data.split(SEPARATOR)
            print('str(id)={} password={} command={}'.format(id, password, command))

            if id.isdigit():
                print('id - int')
                id = int(id)
            print('id={} password={} command={}'.format(id, password, command))

            if command == 'create':
                print('in if')
                new_player = self.game.append_player()
                print('ok1')
                sending_data = str(new_player.id) + SEPARATOR + str(
                    new_player.password) + SEPARATOR + str(
                    self.game.get_field_around_player(new_player.id, new_player.password))
                print('sending data = ', sending_data)
                conn.send(sending_data.encode())
            elif command[:4:] == 'show':
                print('command = show')
                sending_data = str(self.game.get_field_around_player(id, password))
                print('sending data =', sending_data)
                conn.send(sending_data.encode())
        except BaseException:
            pass


class Player:
    def __init__(self, id, password, x, y):
        self.id = id
        self.password = password
        self.x = x
        self.y = y


class Field:
    def __init__(self, n, m):
        self.n = n
        self.m = m
        self.field_arr = gen_field(n, m)

    def in_field(self, x, y):
        return x in range(self.n) and y in range(self.m)

    def get_elem(self, x, y, get_char=True):
        if self.in_field(x, y):
            if not get_char:
                return self.field_arr[x][y]
            return OBJECTS_CHAR[self.field_arr[x][y]]
        if get_char:
            return OBJECTS_CHAR['abroad']
        return 'abroad'

    def __str__(self):
        ret = ''
        for i in self.field_arr:
            for j in i:
                ret += OBJECTS_CHAR[j]
            ret += '\n'
        return ret

    def set_cell(self, x, y, val):
        self.field_arr[x][y] = val


class Game:
    def __init__(self, n, m):
        self.field = Field(n, m)
        self.players = []
        self.not_used_id = 1
        print(self.field)

    def player_exist(self, id):
        return id in [i.id for i in self.players]

    def get_player_by_id(self, id):
        for i in self.players:
            if i.id == id:
                return i

    def move_player(self, id, password, movex, movey):
        # ERRORS
        if movex > MAX_PLAYER_MOVE_ONE_STEP or movey > MAX_PLAYER_MOVE_ONE_STEP:
            return PLAYER_CANT_MOVE_MORE_THAN
        if not self.player_exist(id):
            return PLAYER_WITH_ID_NOT_FOUND(id)
        if self.get_player_by_id(id).password != password:
            return WRONG_PASSWORD

        # NO ERRORS
        for i in range(len(self.players)):
            if self.players[i].id == id:
                self.players[i].x += movex
                self.players[i].y += movey
                return

    def get_field_around_player(self, id, password):
        # ERRORS
        if not self.player_exist(id):
            return PLAYER_WITH_ID_NOT_FOUND(id)
        if self.get_player_by_id(id).password != password:
            return WRONG_PASSWORD

        # NO ERRORS
        player = self.get_player_by_id(id)
        ret = []
        for i in range(player.x - VISION_LEN - 1, player.x + VISION_LEN):
            ret.append([])
            for j in range(player.y - VISION_LEN - 1, player.y + VISION_LEN):
                ret[-1].append(self.field.get_elem(i, j))
        return ret

    def free_cell(self):
        for i in range(self.field.n):
            for j in range(self.field.m):
                if self.field.get_elem(i, j, get_char=False) == 'empty_cell':
                    return i, j

    def append_player(self):
        x, y = self.free_cell()
        self.players.append(Player(self.not_used_id, gen_password(), x, y))
        self.not_used_id += 1
        self.draw_players()
        return self.players[-1]

    def draw_players(self):
        for i in range(self.field.n):
            for j in range(self.field.m):
                if self.field.get_elem(i, j, get_char=False) == PLAYER_WORD:
                    self.field.set_cell(i, j, EMPTY_WORD)
        for i in self.players:
            self.field.set_cell(i.x, i.y, PLAYER_WORD)


if __name__ == '__main__':
    serv = Server()
    ioloop = asyncio.get_event_loop()
    tasks = [ioloop.create_task(serv.run())]
    wait_tasks = asyncio.wait(tasks)
    ioloop.run_until_complete(wait_tasks)
    ioloop.close()